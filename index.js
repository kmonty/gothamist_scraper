/**
 * Gothamist author page scraper.
 * Created by your fellow fucked freelance journalist Kevin Montgomery.
 * Twitter: @kevinmonty
 * Gitlab: https://gitlab.com/kmonty/gothamist_scraper
 */
var config = require('./config');
var casper = require('casper').create({
  clientScripts: ['./jquery.min.js'],
});

var articles = [];
var i = -1;
var url = config.url;
var titleStrip = config.titleStrip;
var cleanOutput = config.cleanOutput;
var hideDisqus = config.hideDisqus;

function getArticles() {
  var articles = document.querySelectorAll('.main-item-summary-content a');
  return Array.prototype.map.call(articles, function(e) {
    return e.getAttribute('href');
  });
}

function CasperException(message, stack) {
  this.name = 'CasperException';
  this.message = message;
  this.stack = stack;
}

casper.on('error', function(msg, backtrace) {
  this.echo('Exception: ' + msg + backtrace);
  throw new CasperException(msg, backtrace);
});

casper.start(url, function() {
  this.echo('Begin scraping', 'INFO');
  // Wait for the page to be loaded.
  this.waitForSelector('.results-previous-link');
  this.echo('Fetching pages from the page ' + url);
});

casper.page.paperSize = {
  width: '11in',
  height: '8.5in',
  orientation: 'portrait',
  border: '0.4in'
};

casper.then(function() {
  articles = this.evaluate(getArticles);

  var nextUrl = this.evaluate(function() {
    var nextLink = document.querySelector(".results-previous-link.next a");
    return nextLink && nextLink.href;
  });

  if (nextUrl) {
    casper.thenOpen(nextUrl, recursiveUrlScraper);
  }
});

function recursiveUrlScraper() {
  this.evaluate(function() {
    // Make sure there are search results.
    if ($('.search-results-container').is(':empty')) {
      this.echo('Finished link scraping. ' + articles.length + ' articles found.', 'INFO');
    }
  });

  articles = articles.concat(this.evaluate(getArticles));

  var nextUrl = this.evaluate(function() {
    var nextLink = document.querySelector(".results-previous-link.next a");
    return nextLink && nextLink.href;
  });

  if (nextUrl) {
    casper.thenOpen(nextUrl, recursiveUrlScraper);
  }
}

casper.then(function() {
  this.echo('Finished link scraping. ' + articles.length + ' articles found.', 'INFO');
});

casper.then(function() {
  this.each(articles, function() { 
    i++;
    this.thenOpen((articles[i]), function() {
      // Prep the title.
      var title = this.getTitle();
      // Remove unwanted common string from title.
      title = title.replace(titleStrip, "");
      var originalTitle = title;
      // Remove all special characters from the title.
      title = title.replace(/[^a-zA-Z ]/g, "");
      title = title.toLowerCase();
      // Replace spaces with underscores.
      title = title.split(' ').join('_');

      // Fetch a timestamp.
      datestamp = this.evaluate(function() {
        stamp = $('abbr.published')[0].innerHTML;
        return stamp;
      });
      var date = new Date(datestamp);

      // Clean up the print page, if desired.
      if (cleanOutput) {
        this.evaluate(function() {
          $('#skyscraper').remove();
          $('#newHeader').remove();
          $('#entry-footer').remove();
          $('#related-module').remove();
          $('.share-buttons').remove();
          $('.entry-tags').remove();
        });
      }
      
      if (hideDisqus) {
        this.evaluate(function() {
          $('#comments-content').remove();
        });
      }

      // Prep the filename we want to save the PDF as.
      var filename = date.getFullYear() + '_' + ('0' + (date.getMonth() + 1)).slice(-2) + '_' + ('0' + (date.getDay() + 1)).slice(-2) + '_' + title + '.pdf';
      this.echo('Saving "' + originalTitle + '"');
      this.echo(' - Saving as: ' + filename)
      this.capture(filename);
    });
  });
});

casper.run(function() {
  this.echo('Done!', 'INFO');
});