module.exports = {
  // Provide the full URL to the author profile you want to pull from.
  url: 'http://sfist.com/author/Jack%20Morse',
  // Optional: pull out the sitename from the article title before saving the article.
  // Another example is ": Gothamist".
  // Leave as-is if you don't know what to do.
  titleStrip: ': SFist',
  // Removes extraneous content before saving as the PDF, such as header
  // menus, recommended links etc.
  // Set to false if undesired.
  cleanOutput: true,
  // Removes Disqus comments before saving the PDF.
  // Defaults to false. Set to true to remove the comments.
  hideDisqus: false,
};