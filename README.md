# Gothamist PDF Scraper
Want to download all your old Gothamist articles before some billionaire forgets to pay the server bill? Behold: a simple CasperJS page scraper that takes your Gothamist author profile and bulk saves all your articles as PDFs.

PDFs are saved in the [year]_[month]_[day]_[title].pdf format. You can optionally clean up the markup on the page (such as removing the header menus, sharing icons, and outbrain links) and remove Disqus comments in the configuration file.

This tool was authored by your fellow journalist Kevin Montgomery.
Limited support available via Twitter [@kevinmonty](https://twitter.com/kevinmonty).

## Caveats
1. This scraper tutorial assumes you are using Mac OS and have a basic level of proficiency.
- Each PDF averages around 150kb. If you have thousands of articles, be sure to have plenty of disk space available before firing this up.
- You should be on a fast/stable connection when scraping, as it can take an excruciatingly long time over a low-bandwidth connection.

## How to Use this Scraper
1. First, you need to install [CasperJS](http://casperjs.org/) via [Homebrew](https://brew.sh/). If you don't know what this means, you doubtfully have them installed. Skip to step two if you already are up and running.
  1. Open terminal.app
  - Install Homebrew (a package manager to Mac OS) by entering the following command: ```/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"```
  - Install PhantomJS (a CasperJS dependency) by entering the command (note: this can take a while to complete): ```brew install phantomjs```
  - Install CasperJS by entering the command: ```brew install casperjs```
- [Download the Gothamist Scraper](https://gitlab.com/kmonty/gothamist_scraper/repository/master/archive.zip)
- Unzip the scraper package, move the unzipped folder into your Documents folder, and rename the gothamist_scraper-master-* folder to ```gothamist_scraper```
- Go into the gothamist_scraper folder. Open ```config.js``` in any text editor. Change the URL field to the author profile you wish to scrape. Save.
- Go back to Terminal.app
- Type the command ```cd ~/Documents/gothamist_scraper/```
- You are ready to begin scraping. You can begin the process by entering the command: ```casperjs index.js```
- The process should take anywhere from a few minutes (for people with a few articles) to hours.
- To halt the scraping at any time (in case of an emergency), you can press Control+C in Terminal.app or close the app entirely.

## The Future
1. The scraper generates the PDF as soon as the page downloads. Unfortunately, this doesn't allow for 3rd-party embeds to render, so they are not outputted (Twitter embeds, Disqus). I need to fix this bug.
- Going to extend this to Gawker/Kinja so I can personally download all my old articles before they are inevitably taken offline :(

### Acknowledgment
Thanks to former SFist scribe [Jack Morse](https://twitter.com/jmorse_/) for guinea pigging this.
